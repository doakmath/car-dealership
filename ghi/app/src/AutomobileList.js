import { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'

function AutomobileList() {

    const [automobiles, setAutomobiles] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8100/api/automobiles/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
            }
        } catch (error) {
            console.error("Error retrieving automobiles: fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    async function deleteAutomobile(autoHref) {
        const url = `http://localhost:8100${autoHref}`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("Error deleting Automobile: deleteAutomobileustomer", error)
        }
    }

    return(
        <div className="my-5 container">
            <div className="row">
                <h1>Automobiles</h1>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                        <th>Year</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>VIN</th>
                        <th>Picture</th>
                        <th>Color</th>
                        <th>Sold</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => {
                    return (
                        <tr key={ automobile.id }>
                            <td>{ automobile.year}</td>
                            <td>{ automobile.model.manufacturer.name }</td>
                            <td>{ automobile.model.name }</td>
                            <td>{ automobile.vin}</td>
                            <td><img src={ automobile.model.picture_url } alt="a picture of a car" style={{ width: '95px', height:'75px'}}></img></td>
                            <td>{ automobile.color }</td>
                            <td>{ automobile.sold.toString().toUpperCase() }</td>
                            {/* <td><button>T or F</button></td> */}
                            {/* <td><Link to="/y/detail">Details/Update</Link></td> */}
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteAutomobile(automobile.href)} >Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )

}

export default AutomobileList
