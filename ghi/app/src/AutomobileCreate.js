import { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'

function AutomobileCreate() {

    const [models, setModels] = useState([])
    const [model, setModel] = useState("")

    let initialData = {
        "year": 0,
        "vin": "",
        "color": "",
        "model_id": model
    }

    let [formData, setFormData] = useState(initialData)

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({...formData, [inputName]: value})
    }

    const fetchModelData = async () => {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setModels(data.models)
        }
    }

    const handleModel = async (event) => {
        const value = event.target.value
        setModel(value)
    }


    useEffect( () => { fetchModelData() }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = "http://localhost:8100/api/automobiles/"
        formData.model_id = model
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
        },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newAutomobile = await response.json()
            let clearedData = {
                "year": 0,
                "vin": "",
                "color": "",
                "model_id": 0
            }
            setFormData(clearedData)
            created()
            window.location.reload()
        } else {
            console.log("ELSE in handleSubmit just triggered")
        }
    }

    function created() {
        return (
            alert(`Well Done!  You created a new Automobile! \nKeep up the good work!`)
        )
    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Automobile</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="mb-3">
                            <select value={model} onChange={handleModel} required id="model_id" name="model_id" className="form-select">
                            <option value="">Choose Make and Model</option>
                            {models.map(model => {
                                return (
                                <option key={model.id} value={model.id}>{model.manufacturer.name} {model.name}</option>
                                )})}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Year" type="number" name="year" id="year" className="form-control" />
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Color" type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default AutomobileCreate
