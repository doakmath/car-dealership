import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './SalespersonList';
import SalespersonCreate from './SalespersonCreate';
import CustomerList from './CustomerList';
import CustomerCreate from './CustomerCreate';
import SalesList from './SalesList';
import SaleCreate from './SaleCreate';
import SalespersonHistory from './SalespersonHistory';
import AutomobileList from './AutomobileList';
import TechnicianForm from './TechnicianForm';
import Technicians from './Technicians';
import AppointmentForm from './AppointmentForm';
import Appointments from './Appointments';
import ServiceHistory from './ServiceHistory';
import Manufacturers from './Manufacturers';
import ManufacturerForm from './ManufacturerForm';
import VehicleModels from './VehicleModels';
import VehicleModelForm from './VehicleModelForm';
import AutomobileCreate from './AutomobileCreate';
import SaleSearch from './SaleSearch';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
            <Route path="/salesperson/history" element={<SalespersonHistory />} />
            <Route path="/salesperson/list" element={<SalespersonList />} />
            <Route path="/salesperson/create" element={<SalespersonCreate />} />
            <Route path="/customer/list" element={<CustomerList />} />
            <Route path="/customer/create" element={<CustomerCreate />} />
            <Route path="/sales/list" element={<SalesList />} />
            <Route path="/sales/create" element={<SaleCreate />} />
            <Route path="/automobile/list" element={<AutomobileList />} />
            <Route path="/automobile/create" element={<AutomobileCreate />} />
            <Route path="/technician/add" element={<TechnicianForm />} />
            <Route path="/technician/list" element={<Technicians />} />
            <Route path="/appointment/create" element={<AppointmentForm />} />
            <Route path="/appointment/list" element={<Appointments />} />
            <Route path="/services/history" element={<ServiceHistory />} />
            <Route path="/manufacturer/list" element={<Manufacturers />} />
            <Route path="/manufacturer/add" element={<ManufacturerForm />} />
            <Route path="/models/list" element={<VehicleModels />} />
            <Route path="/models/add" element={<VehicleModelForm />} />
            <Route path="/SaleSearch" element={<SaleSearch />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
