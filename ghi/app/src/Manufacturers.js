import React, {useState, useEffect} from 'react';

function Manufacturers() {
    const [manufacturers, setManufacturers] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const { manufacturers } = await response.json();
            setManufacturers(manufacturers);
        } else {
            console.error('An error occurred fetching the data')
        }
    }
    const handleDelete = async (id) => {
        const deleteUrl = `http://localhost:8100/api/manufacturers/${id}`
        await fetch(deleteUrl, {method: 'delete'})
        getData()
    }
    useEffect(() => {
        getData()
    }, []);

    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Manufacturers</h1>

                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers.map(manufacturer => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td>{manufacturer.name}</td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => {handleDelete(manufacturer.id)}}>Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Manufacturers;
