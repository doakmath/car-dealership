import React from 'react';
import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-dark bg-dark fixed-top">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Car Dealership</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasDarkNavbar" aria-controls="offcanvasDarkNavbar" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="offcanvas offcanvas-end bg-dark text-white" tabIndex="-1" id="offcanvasDarkNavbar" aria-labelledby="offcanvasDarkNavbarLabel">
          <div className="offcanvas-header">
            <h5 className="offcanvas-title" id="offcanvasDarkNavbarLabel">Department Navigation</h5>
            <button type="button" className="btn-close btn-close-white" data-bs-dismiss="offcanvas" aria-label="Close"></button>
          </div>
          <div className="offcanvas-body">
            <ul className="navbar-nav justify-content-end flex-grow-1 pe-3">
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Sales</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="salesDropdown">
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/salesperson/list">Salespeople</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/salesperson/create">Add Salesperson</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/customer/list">Customers</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/customer/create">Add a Customer</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/sales/list">Sales</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/sales/create">Create a Sale</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/salesperson/history">Salesperson History</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="servicesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Services</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="servicesDropdown">
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/technician/add">Add a Technician</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/appointment/create">Add a Service Appointment</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/technician/list">Technicians List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/appointment/list">Service Appointments List</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/services/history">Service History</NavLink>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown">
                <span className="nav-link dropdown-toggle" id="inventoryDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">Inventory</span>
                <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="inventoryDropdown">
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/manufacturer/list">Manufacturers</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/manufacturer/add">Add a Manufacturer</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/models/list">Vehicle Models</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/models/add">Add a Vehicle Model</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/automobile/list">Automobiles</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link dropdown-item" to="/automobile/create">Add an Automobile</NavLink>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
)
}

export default Nav;
