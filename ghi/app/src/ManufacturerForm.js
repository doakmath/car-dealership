import React, { useState } from 'react';

function ManufacturerForm() {

    const [name, setName] = useState('');



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name };

        const manufacturerURL = 'http://localhost:8100/api/manufacturers/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturerURL, fetchOptions);
        if (response.ok) {
            const newManufacturer = await response.json();
            setName('');

            window.location.reload();
        }
    }

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="add-manufacturer-form">
                        <h1 className="card-title">Add a Manufacturer</h1>
                        <div className="col">
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeName} required placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                    <label htmlFor="first_name">Name</label>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerForm;
