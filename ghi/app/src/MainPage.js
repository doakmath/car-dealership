import React from 'react';
import carImage from "./assets/car.png"
import './MainPage.css';

function MainPage() {

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Car Dealership</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The premiere solution for automobile dealership
          management!
        </p>
        <div className="animationContainer">
          <div className="smoke"></div>
          <img src={carImage} alt="Car" width="200" height="100" className="car-animation" />
        </div>
      </div>
    </div>
  );
}

export default MainPage;
