import React, { useEffect, useState } from 'react';
import moment from 'moment';

function AppointmentForm() {

    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [reason, setReason] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [technician, setTechnician] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const dateTime = moment.utc(`${date} ${time}`).format();
        const data = { vin, customer, reason, technician: technician, date_time: dateTime, status: "Pending" };

        const appointmentURL = 'http://localhost:8080/api/appointments/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appointmentURL, fetchOptions);

        if (response.ok) {
            const newAppointment = await response.json();
            console.log(newAppointment)

            setVin('');
            setCustomer('');
            setDate('');
            setTime('');
            setReason('');
            setTechnician('');

            window.location.reload();
        }
    }

    const handleChangeVIN = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    const handleChangeCustomer = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }
    const handleChangeDate = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleChangeTime = (event) => {
        const value = event.target.value;
        setTime(value);
    }
    const handleChangeReason = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleChangeTechnician = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <h1 className="card-title">Create a Service Appointment</h1>
                        <div className="col">
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeVIN} required placeholder="VIN" type="text" id="vin" name="vin" className="form-control" />
                                    <label htmlFor="vin">VIN</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeCustomer} required placeholder="Customer" type="text" id="customer" name="customer" className="form-control" />
                                    <label htmlFor="customer">Customer</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeDate} value={date} required placeholder="Date" type="date" id="date" name="date" className="form-control" />
                                    <label htmlFor="date">Date</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeTime} value={time} required placeholder="Time" type="time" id="time" name="time" className="form-control" />
                                    <label htmlFor="time">Time</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeReason} required placeholder="Reason" type="text" id="reason" name="reason" className="form-control" />
                                    <label htmlFor="reason">Reason</label>
                                </div>
                            </div>
                            <div className="col">
                                <div className="mb-3">
                                    <select onChange={handleChangeTechnician} value={technician} name="technician" required id="technician" className="form-select" >
                                        <option value="">Choose a Technician</option>
                                        {technicians.map(technician => (
                                            <option key={technician.id} value={technician.id}>{`${technician.first_name} ${technician.last_name}`}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button className="btn btn-lg btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
