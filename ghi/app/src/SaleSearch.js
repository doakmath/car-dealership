import { useState, useEffect } from 'react'
import Datetime from 'react-datetime';
import { Link } from 'react-router-dom';


function SaleSearch () {

    const initialService = {"appointments": [{"id": 0}]}

    const [dateTime, setDateTime] = useState("")
    const [serviceData, setServiceData] = useState(initialService)



    const handleDateTime = (event) => {
        const valueDate = event._d
        const readableValueDate = valueDate.toLocaleString()
        setDateTime(readableValueDate)
    }

    const handleDate = (event) => {
        const value = event.target.value
        setDateTime(value)
    }

    const fetchServiceData = async () => {
        const serviceDataUrl = "http://localhost:8080/api/appointments/"
        const serviceDataResponse = await fetch(serviceDataUrl)
        if (serviceDataResponse.ok) {
            const serviceData = await serviceDataResponse.json()
            setServiceData(serviceData)
        }
    }

    useEffect(() => {
        fetchServiceData()
    }, [])

    let inputProps = {
        placeholder: 'Select a Date and Time on the Calendar Below',
        disabled: true,
        onMouseLeave: () => alert('HEY!!! \nDid Krissie or Mathew tell you about this page...?  How did you get here?\nWell, thanks for ending up here and checking out our site.\nWe worked really hard on it!!!')
    };


    // handleChanger = (event) => {
    //     const value = event.target.value
    //     setChanger(value)
    // }


    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Select a Service Record by Date Time</h1>
            <div>
                <Datetime inputProps={ inputProps } onChange={handleDateTime} />
            </div>
            <div>
                <h3>The list will populate with any Services that occurred on the date time below:</h3>
            <div className="alert alert-secondary" role="alert">
                {dateTime}
            </div>
            <div>
                <h3>Manual Date Input Below:</h3>
                <h5>MM/DD/YYYY HH:MM A/PM</h5>
            </div>
                <input onChange={handleDate}/>
                <p>00/00/0000 00:00 |A/P|M</p>
            <div>
                <div className="my-5 container">
                    <div className="row">
                        <h1>service record by date</h1>
                        <Link to="/services/history">Click to see ALL Service Records</Link>
                        <table className="table table-striped m-3">
                        <thead>
                            <tr>
                                <th>APPOINTMENT ID</th>
                                <th>date_time</th>
                                <th>Customer</th>
                                <th>Status</th>
                                <th>Technician/ID</th>
                                <th>VIN</th>
                                <th>Reason</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {serviceData.appointments.filter(appointment => Date.parse(appointment.date_time) === Date.parse(dateTime)).map(appointment => {
                                return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.id}</td>
                                        <td>{ appointment.date_time}</td>
                                        <td>{ appointment.customer }</td>
                                        <td>{ appointment.status }</td>
                                        <td>{ appointment.technician.first_name } { appointment.technician.last_name } #{ appointment.technician.employee_id }</td>
                                        <td>{ appointment.vin }</td>
                                        <td>{ appointment.reason }</td>
                                        <td></td>
                                        <td></td>
                                </tr>
                                )
                            })}
                        </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    )
}

export default SaleSearch
