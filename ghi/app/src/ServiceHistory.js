import React, { useState, useEffect } from 'react';
import moment from 'moment';


function Appointments() {
    const [appointments, setAppointments] = useState([]);
    const [filterValue, setFilterValue] = useState('');

    const getData = async (value=null) => {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            const automobileResponse = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok && automobileResponse.ok) {
                const appointmentsData = await response.json();
                const automobileData = await automobileResponse.json();

                const filteredList = appointmentsData.appointments.filter(appointment => appointment.vin === value).map(appointment => appointment)

                const vipVINs = new Set(automobileData.autos.map(auto => auto.vin));

                const vipAppointments = appointmentsData.appointments.map(appointment => {
                    const isVIP = vipVINs.has(appointment.vin);

                    const localDateTime = moment(appointment.date_time).local();

                    return {
                        id: appointment.id,
                        vin: appointment.vin,
                        customer: appointment.customer,
                        date: localDateTime.format("MMM Do YYYY"),
                        time: localDateTime.format("h:mm:ss A"),
                        technician: appointment.technician,
                        reason: appointment.reason,
                        status: appointment.status,
                        isVIP: isVIP
                    };
                });
                if (value) {
                    setAppointments(filteredList)
                } else {
                    setAppointments(vipAppointments)
                }

            } else {
                if (!response.ok) console.error('An error occurred fetching appointments');
                if (!automobileResponse.ok) console.error('An error occurred fetching automobileVO data');
            }
        } catch (error) {
            console.error('An error occurred:', error);
        }
    };

useEffect(() => {
    getData();
}, []);

const handleInputChange = (event) => {
    setFilterValue(event.target.value);
};

const handleSubmit = (event) => {
    event.preventDefault();
    getData(filterValue);
};


    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Service History</h1>
                <form onSubmit={handleSubmit} className="input-group mb-3">
                    <input onChange={handleInputChange} type="text" className="form-control" placeholder="Search by VIN" aria-label="Search by VIN" />
                    <div className="input-group-append">
                        <button className="btn btn-outline-secondary" type="submit">Search</button>
                    </div>
                </form>
                <table className="table table-striped m-3">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Is VIP?</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.isVIP ? 'Yes' : 'No'}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointment.status}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default Appointments;
