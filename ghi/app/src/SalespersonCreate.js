import { useState, useEffect } from 'react'
import { Link, useLocation } from 'react-router-dom'

function SalespersonCreate() {

    let initialData = {
        "first_name": "",
        "last_name": "",
        "employee_id": 0
    }

    let [formData, setFormData] = useState(initialData)

    const handleFormChange = (event) => {
        const value = event.target.value
        const inputName = event.target.name
        setFormData({...formData, [inputName]: value})
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const url = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
        },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            const newSalesperson = await response.json()
            let clearedData = {
                "first_name": "",
                "last_name": "",
                "employee_id": 0
            }
            setFormData(clearedData)
            created()
            window.location.reload()
        } else {
            console.log("ELSE in handleSubmit just triggered")
        }
    }

    function created() {
        return (
            alert(`Nice!  You created a new Salesperson! \nKeep up the good work!`)
        )
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Salesperson</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Employee ID" type="number" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalespersonCreate
