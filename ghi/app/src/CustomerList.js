import { useState, useEffect } from 'react'


function CustomerList() {

    const [customers, setCustomers] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/customers/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
            }
        } catch (error) {
            console.error("Error retrieving customers: fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    async function deleteCustomer(customerId) {
        const url = `http://localhost:8090/api/customers/${customerId}/`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("Error deleting customer: deleteCustomer", error)
        }
    }


    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Customers</h1>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>View/Update</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                    return (
                        <tr key={ customer.id }>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.address }</td>
                            <td>{ customer.phone_number }</td>
                            <td><button>Details</button></td>
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteCustomer(customer.id)} >Delete</button></td>
                        </tr>
                    );
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )
}

export default CustomerList
