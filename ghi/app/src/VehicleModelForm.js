import React, { useEffect, useState } from 'react';

function VehicleModelForm() {

    const [name, setName] = useState('');
    const [picture_url, setPictureUrl] = useState('')
    const [manufacturers, setManufacturers] = useState([])
    const [selectedManufacturer, setSelectedManufacturer] = useState('')

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name, picture_url, manufacturer_id: selectedManufacturer};

        const vehiclemodelURL = 'http://localhost:8100/api/models/'
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        try {
            const response = await fetch(vehiclemodelURL, fetchOptions);
            if (response.ok) {
                const newVehicleModel = await response.json();
                setName('');
                setPictureUrl('');
                setSelectedManufacturer('');

                window.location.reload();

            } else {
                console.error('Submission failed', await response.text());
            }
        } catch (error) {
            console.error('An error occurred', error);
        }
    }

    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleChangePicture = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setSelectedManufacturer(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <form onSubmit={handleSubmit} id="add-vehicle-model-form">
                        <h1 className="card-title">Add a Vehicle Model</h1>
                        <div className="col">
                            <div className="col">
                                <div className="form-floating mb-3">
                                    <input onChange={handleChangeName} required placeholder="Name" type="text" id="name" name="name" className="form-control" />
                                    <label htmlFor="name">Name</label>
                                </div>
                            </div>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangePicture} required placeholder="Picture URL" type="text" id="picture_url" name="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={selectedManufacturer} onChange={handleChangeManufacturer} required name="manufacturers" id="manufacturers" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-lg btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleModelForm;
