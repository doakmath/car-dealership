import { useState, useEffect } from 'react'

function SalesList() {

    const [sales, setSales] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8090/api/sales/"
        const response = await fetch(url);
        try {
            if (response.ok) {
            const data = await response.json()
            setSales(data.sales)
            }
        } catch (error) {
            console.error("Error retrieving sales: fetchData", error)
        }
    }

    useEffect(() => {
        fetchData()
    }, []);

    async function deleteSale(SaleId) {
        const url = `http://localhost:8090/api/sales/${SaleId}/`
        const fetchConfig = {method: "DELETE"}
        const response = await fetch(url, fetchConfig)
        try {
            if (response.ok) {
                fetchData()
            }
        } catch (error) {
            console.error("Error deleting sale: deleteSale", error)
        }
    }



    return (
        <div className="my-5 container">
            <div className="row">
                <h1>Sales</h1>
                <table className="table table-striped m-3">
                <thead>
                    <tr>
                        <th>Automobile</th>
                        <th>VIP Status</th>
                        <th>Sold</th>
                        <th>Customer</th>
                        <th>Salesperson</th>
                        <th>Price</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                    return (
                        <tr key={ sale.id }>
                            <td>VIN: { sale.automobile.vin }</td>
                            <td>Bool</td>
                            <td>{ sale.automobile.sold.toString().toUpperCase() }</td>
                            <td>Name: {sale.customer.first_name} { sale.customer.last_name }, Phone: { sale.customer.phone_number }</td>
                            <td>Name: {sale.salesperson.first_name} {sale.salesperson.last_name} #ID: { sale.salesperson.employee_id }</td>
                            <td>${ sale.price }</td>
                            <td><button className="btn btn-outline-danger end" onClick={ () => deleteSale(sale.id)}>Delete</button></td>
                        </tr>
                    )
                    })}
                </tbody>
                </table>
            </div>
        </div>
    )
}

export default SalesList
