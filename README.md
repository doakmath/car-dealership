# CarCar

Team:

Krissie Rivera- Services Microservice
Mathew Doak- Sales Microservice

## Beginning

Ensure you have the proper programs installed before you begin:
- Docker
- Git
- Node.js (most current version)

Retireving the project from Git:
- For the respository https://www.gitlab/krissie.rivera/project-beta
- Clone the repository to you local computer with the comman: git clone <<repostiory.url>>
- Change into the directory that we cloned the project which is also called the "working directorty:" cd <<name of project>>
- Build the containers and images in Docker with the following commands:
    'docker volume create beta-beta'
    'docker-compose build'
    'docker-compose up'

Below you will see all the containers running in Docker:
![alt text](docker.png)

This process may take awhile, run to the restroom grab your water but don't got too far because after Docker is smoothly running go to: http://localhost:3000/ to see the application UI

![alt text](localhost.png)

## Design

![alt text](DDDCarCar.png)

## Inventory Microservice

This microservice includes the inventory lists of vehicles and three indepenedent models:
- Manufacturer
- Vehicle
- Automobile

Manufacturer API Endpoints:

GET     List Manufacturerers        http://localhost:8100/api/manufacturers/
POST    Create a Manufacturer       http://localhost:8100/api/manufacturers/
GET     Get a Manufacturer          http://localhost:8100/api/manufacturers/int:pk/
PUT     Update a Manfacturer        http://localhost:8100/api/manufacturers/int:pk/
DELETE  Delete a Manufacturer       http://localhost:8100/api/manufacturers/int:pk/

- List Manufacturers: No data needs to be sent when accessing the endpoint URL. The application returns the following JSON object. It includes the followng properties: "href", "id", "name":

{
    "href": "/api/manufacturers/4/",
    "id": 4,
    "name": "BMW"
}

- Create a Manufacturer: Data needs to be sent like the example below in the JSON body in order to send this endpoint URL. It only requires one property: "name":

{
    "name": "Chrysler"
}

A successful request will return with the three properties we saw in (List Manufacturers). The "href" and "id" property are created automatically:

{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}

- Get a Manufacturer: This allows the detailed view of a specific manufacturer. The "id" is required at the endpoint URL to send the request, no other data is needed in the JSON body:

http://localhost:8100/api/manufacturers/1/

A successful request will appear like the example of "Chrysler" above.

- Update a Manufacturer: to update a specific manufacturer, send a PUT request with the updated "name" in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific manufacturer you are updating:

{
    "name": "Volvo"
}

example URL: http://localhost:8100/api/manufacturers/1/

A successful return will produce the following (notice the "id" stayed the same but the "name" was changed):

{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Volvo"
}

- Delete a Manufacturer: to delete a specific manufacturer, send a DELETE request, no data needed in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific manufacturer you are deleting:

example URL: http://localhost:8100/api/manufacturers/1/

{
	"id": null,
	"name": "Volvo"
}

A successful deletion will have the "id" show up as null.

Vehicle Models API Endpoints:

GET     List Vehicle Models         http://localhost:8100/api/models/
POST    Create a Vehicle Model      http://localhost:8100/api/models/
GET     Get a Vehicle Model         http://localhost:8100/api/models/int:pk/
PUT     Update a Vehicle Model      http://localhost:8100/api/models/int:pk/
DELETE  Delete a Vehicle Model      http://localhost:8100/api/models/int:pk/

- List Vehicle Models: No data needs to be sent when accessing the endpoint URL. The application returns the following JSON object. It includes the followng properties: "href", "id", "picture_url", "manufacturer":

{
    "href": "/api/models/2/",
    "id": 2,
    "name": "i8",
    "picture_url": "https://1.bp.blogspot.com/-jWBIFsChc9E/XuI9qEPX7eI/AAAAAAAAFpI/unR9w-ZUj0AsH5LpSGo5TCokWmAisPFawCK4BGAsYHg/s1920/bmw-i8_100634676_h.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/4/",
        "id": 4,
        "name": "BMW"
    }
}

- Create a Vehicle Model: Data needs to be sent like the example below in the JSON body in order to send this endpoint URL. It requires the following properties: "name", "picture_url", "maufacturer_id":

{
    "name": "i5",
    "picture_url": "https://www.motortrend.com/uploads/2023/09/002-2024-BMW-i5-front-three-quarters-in-action.jpg?fit=around%7C875:492",
    "manufacturer_id": 4
}

A successful request will return with the properties we saw in (List Vehicle Models). The "href" and "id" property are created automatically:

{
	"href": "/api/models/11/",
	"id": 11,
	"name": "i5",
	"picture_url": "https://www.motortrend.com/uploads/2023/09/002-2024-BMW-i5-front-three-quarters-in-action.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "BMW"
	}
}

- Get a Vehicle Model: This allows the detailed view of a specific vehicle model. The "id" is required at the endpoint URL to send the request, no other data is needed in the JSON body:

http://localhost:8100/api/models/1/

A successful request will appear like the example of "BMW" above.

- Update a Vehicle Model: to update a specific vehicle model, send a PUT request with the updated property/ies in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific vehicle model you are updating:

{
    "name": "i8"
}

example URL: http://localhost:8100/api/models/11/

A successful return will produce the following (notice the "id" stayed the same but the "name" was changed):

{
	"href": "/api/models/11/",
	"id": 11,
	"name": "i8",
	"picture_url": "https://www.motortrend.com/uploads/2023/09/002-2024-BMW-i5-front-three-quarters-in-action.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "BMW"
	}
}

- Delete a Vehicle Model: to delete a specific vehicle model, send a DELETE request, no data needed in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific vehicle model you are deleting:

example URL: http://localhost:8100/api/models/11/

{
	"href": "/api/models/11/",
	"id": null,
	"name": "i8",
	"picture_url": "https://www.motortrend.com/uploads/2023/09/002-2024-BMW-i5-front-three-quarters-in-action.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "BMW"
	}
}

A successful deletion will have the "id" show up as null.

Automobiles API Endpoints:

NOTE: Take note that the endpoint URLS identify the automobiles by "vin" and not by their "id". The "vin" is the unique identifier for these objects.

GET     List Automobiles          http://localhost:8100/api/automobiles/
POST    Create an Automobile      http://localhost:8100/api/automobiles/
GET     Get an Automobile         http://localhost:8100/api/automobiles/int:vin/
PUT     Update an Automobile      http://localhost:8100/api/automobiles/int:vin/
DELETE  Delete an Automobile      http://localhost:8100/api/automobiles/int:vin/

- List Automobiles: No data needs to be sent when accessing the endpoint URL. The application returns the following JSON object. It includes the followng properties: "href", "id", "color", "year", "vin", "sold" (a boolean value):

{
    "href": "/api/automobiles/JHMCG5547XC803932/",
    "id": 2,
    "color": "Red",
    "year": 2023,
    "vin": "JHMCG5547XC803932",
    "model": {
        "href": "/api/models/12/",
        "id": 12,
        "name": "Civic",
        "picture_url": "https://hips.hearstapps.com/hmg-prod/images/img-7295-10-1666813703.jpg?crop=0.7498493068113322xw:1xh;center,top&resize=1200:*",
        "manufacturer": {
            "href": "/api/manufacturers/5/",
            "id": 5,
            "name": "Honda"
        }
    },
    "sold": false
},

- Create an Automobile: Data needs to be sent like the example below in the JSON body in order to send this endpoint URL. It requires the following properties: "color", "year", "vin", "model_id":

{
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
}

A successful request will return with the properties we saw in (List Automobiles). The "href" and "id" property are created automatically:

{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}

- Get an Automobile: This allows the detailed view of a specific automobile. The "vin" is required at the endpoint URL to send the request, no other data is needed in the JSON body:

http://localhost:8100/api/automobiles/JHMCG5547XC803932/

A successful request will appear like the example of "Chrysler" above.

- Update an Automobile: to update a specific vehicle model, send a PUT request with the updated property/ies in the JSON body. Ensure that the endpoint URL has the correct "vin" of the specific automobile you are updating:

{
    "color": "blue",
    "year": 1999
}

example URL: http://localhost:8100/api/automobiles/JHMCG5547XC803932/

A successful return will produce the following (notice the "id" stayed the same but the "name" was changed):

{
    "href": "/api/automobiles/JHMCG5547XC803932/",
    "id": 2,
    "color": "blue",
    "year": 1999,
    "vin": "JHMCG5547XC803932",
    "model": {
        "href": "/api/models/12/",
        "id": 12,
        "name": "Civic",
        "picture_url": "https://hips.hearstapps.com/hmg-prod/images/img-7295-10-1666813703.jpg?crop=0.7498493068113322xw:1xh;center,top&resize=1200:*",
        "manufacturer": {
            "href": "/api/manufacturers/5/",
            "id": 5,
            "name": "Honda"
        }
    },
    "sold": false
}

- Delete an Automobile: to delete a specific automobile, send a DELETE request, no data needed in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific automobile you are deleting:

example URL: http://localhost:8100/api/automobiles/2/

{
    "href": "/api/automobiles/JHMCG5547XC803932/",
    "id": null,
    "color": "blue",
    "year": 1999,
    "vin": "JHMCG5547XC803932",
    "model": {
        "href": "/api/models/12/",
        "id": 12,
        "name": "Civic",
        "picture_url": "https://hips.hearstapps.com/hmg-prod/images/img-7295-10-1666813703.jpg?crop=0.7498493068113322xw:1xh;center,top&resize=1200:*",
        "manufacturer": {
            "href": "/api/manufacturers/5/",
            "id": 5,
            "name": "Honda"
        }
    },
    "sold": false
}

A successful deletion will have the "id" show up as null.

## Service Microservice

The services micoservices is used to keep track of all service appointments through creating appointments, a list of pending appointments that allows you to individually cancel and finish each one, and a history list of all statuses of appointments. To create an appointment, the instances of technician must be created, as it calls on that information to properly submit. It includes three models:
- Technician
- Appointment
- AutomobileVO (populated from polled data from inventory microservice)

Technician API Endpoints:

GET     List Technicians        http://localhost:8080/api/technicians/
POST    Create a Technician     http://localhost:8080/api/technicians/
DELETE  Delete a Technician     http://localhost:8080/api/technicians/int:pk/

- List Techncicians: No data needs to be sent when accessing the endpoint URL. The application returns the following JSON object. It includes the followng properties: "first_name", "last_name", "employee_id", "id":

{
    "technicians": [
		{
			"href": "/api/technicians/5/",
			"id": 5,
			"first_name": "Bob",
			"last_name": "Schneitzel",
			"employee_id": "1"
		},
		{
			"href": "/api/technicians/7/",
			"id": 7,
			"first_name": "Bob",
			"last_name": "Heart",
			"employee_id": "2"
		},
    ]
}

- Create a Technician: Data needs to be sent like the example below in the JSON body in order to send this endpoint URL. It requires the following properties: "first_name", "last_name", "id":

{
    "first_name": "Matt",
    "last_name": "Road",
    "employee_id": "mroad"
}

A successful request will return with the properties we saw in (List Technicians). The "href" and "id" property are created automatically (not to be confused with employee_id):

{
    "href": "/api/technicians/9/",
    "id": 9,
    "first_name": "Matt",
    "last_name": "Road",
    "employee_id": "mroad"
}

- Delete a Technician: to delete a specific technician, send a DELETE request, no data needed in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific technician you are deleting:

example URL: http://localhost:8080/api/technicians/9/

{
	"Deleted": true
}

Appointments API Endpoints:

GET     List Service Appointments         http://localhost:8080/api/appointments/
POST    Create a Service Appointment      http://localhost:8080/api/appointments/
PUT     Cancel a Service Appointment      http://localhost:8080/api/appointments/int:pk/cancel/
PUT     Finish a Service Appointment      http://localhost:8080/api/appointments/int:pk/finish/

- List Service Appointments: No data needs to be sent when accessing the endpoint URL. The application returns the following JSON object. It includes the followng properties: "date_time", "reason", "vin", "customer", "status", "technician":

{
"appointments": [
    {
        "href": "/api/appointments/5/",
        "id": 5,
        "date_time": "2024-02-10T14:00:00+00:00",
        "reason": "oil change",
        "vin": "5FNRL38739B001353",
        "customer": "Kool Aid Man",
        "status": "Pending",
        "technician": {
            "href": "/api/technicians/7/",
            "id": 7,
            "first_name": "Bob",
            "last_name": "Heart",
            "employee_id": "2"
        }
    },
    {
        "href": "/api/appointments/4/",
        "id": 4,
        "date_time": "2024-02-08T14:00:00+00:00",
        "reason": "brake fluid check up",
        "vin": "1B3HB48B67D562726",
        "customer": "Harvey Dent",
        "status": "Pending",
        "technician": {
            "href": "/api/technicians/7/",
            "id": 7,
            "first_name": "Bob",
            "last_name": "Heart",
            "employee_id": "2"
        }
    }]
}

- Create a Service Appointment: Data needs to be sent like the example below in the JSON body in order to send this endpoint URL. It requires the following properties: "date_time", "reason", "vin", "customer", "technician":

{
	"date_time": "2024-02-12 16:00",
	"reason": "new tires",
	"vin": "JHMCG5547XC803932",
	"customer": "Post Major",
	"status": "Pending",
	"technician": 8
}

A successful request will return with the properties we saw in (List Service Appointments). The "href" and "id" property are created automatically:

{
	"href": "/api/appointments/6/",
	"id": 6,
	"date_time": "2024-02-12 16:00",
	"reason": "new tires",
	"vin": "JHMCG5547XC803932",
	"customer": "Post Major",
	"status": "Pending",
	"technician": {
		"href": "/api/technicians/8/",
		"id": 8,
		"first_name": "Renee",
		"last_name": "Plum",
		"employee_id": "rplum"
	}
}

- Cancel/ Finish a Service Appointment: to cancel or finsih a specific service appointment, send a PUT request to the specific URL (examples below), no data needed in the JSON body. Ensure that the endpoint URL has the correct "id" of the specific manufacturer you are updating:

Cancel example URL: http://localhost:8080/api/appointments/5/cancel/

A successful request will return an updated "status" from "Pending" to "Canceled":

{
    "href": "/api/appointments/5/",
    "id": 5,
    "date_time": "2024-02-10T14:00:00+00:00",
    "reason": "oil change",
    "vin": "5FNRL38739B001353",
    "customer": "Kool Aid Man",
    "status": "Canceled",
    "technician": {
        "href": "/api/technicians/7/",
        "id": 7,
        "first_name": "Bob",
        "last_name": "Heart",
        "employee_id": "2"
    }
}

Finish example URL: http://localhost:8080/api/appointments/4/finish/

A successful request will return an updated "status" from "Pending" to "Finished":

{
    "href": "/api/appointments/4/",
    "id": 4,
    "date_time": "2024-02-08T14:00:00+00:00",
    "reason": "brake fluid check up",
    "vin": "1B3HB48B67D562726",
    "customer": "Harvey Dent",
    "status": "Finished",
    "technician": {
        "href": "/api/technicians/7/",
        "id": 7,
        "first_name": "Bob",
        "last_name": "Heart",
        "employee_id": "2"
    }
}

## Sales microservice
sales/api/sales_rest
port 8090:8000

Our Sales API Microservice handles the data management for the Sales, Customers, and Salespeople.  Each model has the API endpoints listed below.  The Sales API Microservice utilizes a poller function to retrieve data every 60 seconds about the Automobile objects in the Inventory database.  It has the ability to create and list Salespeople and Customers.  It also has a special feature of seeing an individual Salespersons History.

The Models have attributes that populate a table in the database.  The Views are the functions in which we create our RESTful design.  The URLs are the pathways connecting the various modules and ultimately move the request from database to browswer.  The ModelEncoders restrict which data is displayed and passed.

Our React Frontend utilizes these RESTful API endpoints to generate functional forms and lists that can populate dynamically using state hooks.  We created a clean and appealing design for easy navigation utilizing Bootstrap.  Our Frontend dynamically changes several aspects of our data, such as Pending Status changes, and several others.  This is achieved through the manipulation of state using asynchronous fetch requests, and filter, and map functions.  Also, as a complete nonsequitur, there is a hidden component at localhost:3000/salesearch that has another feature; the sale list can be populated by datetime.


|-------------------------------------------------------MICROSERVICE--------------------------------------------------------|

EXAMPLE INPUT Examples are Json body in the request configurations.

-----------------------------------------------------------SALES-------------------------------------------------------------

The Sales endpoints allow for requests to be made to the Sales Model, and data stored or retrieved through the attributes of the Model.  You can retrieve a list, get a single instance, delete an instance, or create a new instance.

| Method | URL |

GET     http://localhost:8090/api/sales/

EXAMPLE INPUT

EXAMPLE OUTPUT

{
	"sales": [
		{
			"id": 41,
			"automobile": {
				"id": 22,
				"vin": "14444231234grg3",
				"sold": true
			},


POST    http://localhost:8090/api/sales/

EXAMPLE INPUT

{
	"automobile": "1C3CC5FB2AN120174",
	"salesperson": 1,
	"customer": 4321,
	"price": 9999
}

EXAMPLE OUTPUT

{
	"id": 10,
	"automobile": {
		"id": 1,
		"vin": "1C3CC5FB2AN120174",
		"sold": false
	},
	"customer": {
		"id": 6,
		"first_name": "m",
		"last_name": "d",
		"address": "m1",
		"phone_number": 4321
	},
	"salesperson": {
		"id": 9,
		"first_name": "m",
		"last_name": "d",
		"employee_id": 1
	},
	"price": 9999
}


GET     http://localhost:8090/api/sales/:id

EXAMPLE INPUT

EXAMPLE OUTPUT

[
	{
		"id": 41,
		"automobile": {
			"id": 22,
			"vin": "14444231234grg3",
			"sold": true
		},
		"customer": {
			"id": 7,
			"first_name": "Guest",
			"last_name": "Guest",
			"address": "Guest",
			"phone_number": "0000000000"
		},
		"salesperson": {
			"id": 2,
			"first_name": "Jane",
			"last_name": "Delacroix",
			"employee_id": "22"
		},
		"price": 1000
	}
]

DELETE  http://localhost:8090/api/sales/:id

EXAMPLE INPUT

EXAMPLE OUTPUT

{
	"deleted:": false
}



--------------------------------------------------CUSTOMERS-----------------------------------------------------------------

The Sales endpoints allow for requests to be made to the Sales Model, and data stored or retrieved through the attributes of the Model.  You can retrieve a list, get a single instance, delete an instance, or create a new instance.


| Method | URL |

GET     http://localhost:8090/api/customers/

EXAMPLE INPUT

EXAMPLE OUTPUT

{
	"customers": [
		{
			"id": 1,
			"first_name": "MaryAnn",
			"last_name": "Jameson",
			"address": "12 Bunker dr Houst TX",
			"phone_number": "2136153378"
		},


POST    http://localhost:8090/api/customers/

EXAMPLE INPUT

{
	"first_name": "Johnny2",
	"last_name": "Knoxville2",
	"address": "123 Johhny Street2",
	"phone_number": 1232
}

EXAMPLE OUTPUT

{
	"id": 13,
	"first_name": "Johnny2",
	"last_name": "Knoxville2",
	"address": "123 Johhny Street2",
	"phone_number": 1232
}


GET     http://localhost:8090/api/customers/:id

EXAMPLE INPUT

EXAMPLE OUTPUT

[
	{
		"id": 2,
		"first_name": "April",
		"last_name": "ONeill",
		"address": "1212 New York st NYC NY",
		"phone_number": "885673522"
	}
]



DELETE  http://localhost:8090/api/customers/:id

EXAMPLE INPUT

EXAMPLE OUTPUT

{
	"deleted:": true
}



------------------------------------------------------------SALESPEOPLE----------------------------------------------------------

The Salespeople endpoints allow for requests to be made to the Salesperson Model, and data stored or retrieved through the attributes of the Model.  You can retrieve a list, retrieve an individual instance, create a new instance, or modify certain attributes, however, foreign keys cannot be modified via the update option.

| Method | URL |

GET     http://localhost:8090/api/salespeople/

EXAMPLE INPUT

EXAMPLE OUTPUT

{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Max",
			"last_name": "Landolphini",
			"employee_id": "11"
		},

POST    http://localhost:8090/api/salespeople/

EXAMPLE INPUT

{
	"first_name": "Kristie",
	"last_name": "Matt",
	"employee_id": 787
}

EXAMPLE OUTPUT

{
	"id": 8,
	"first_name": "Kristie",
	"last_name": "Matt",
	"employee_id": 787
}

GET     http://localhost:8090/api/salespeople/:id

EXAMPLE INPUT

EXAMPLE OUTPUT

PUT     http://localhost:8090/api/salespeople/:id

EXAMPLE INPUT

{
	"first_name": "Kris!!!!!",
	"last_name": "!!!Mat!!",
	"employee_id": 1234
}

EXAMPLE OUTPUT

{
	"id": 2,
	"first_name": "Kris!!!!!",
	"last_name": "!!!Mat!!",
	"employee_id": "1234"
}

DELETE  http://localhost:8090/api/salespeople/:id

----->A paragraph about the Views, URLs, settings, etc.

## Value Objects

Through both Sales and Services, autombile data needs to be called from the Inventory microservice so in each microservice an AutomobileVO is necessary. Each microservice receieves data via the microservice's poller; the data is stored as a new instance of automobileVO object. The data is used to filter, populate, and update certain lists within each mircoservice.
