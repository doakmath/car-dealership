from django.urls import path
from .views import (
    list_salespeople,
    salesperson,
    list_customers,
    customer,
    list_sales,
    sale,
    list_automobileVOs
)

urlpatterns = [
    path("salespeople/", list_salespeople, name="list_salespeople"),
    path("salespeople/<int:pk>/", salesperson, name="salesperson"),
    path("customers/", list_customers, name="list_customers"),
    path("customers/<int:pk>/", customer, name="customer"),
    path("sales/", list_sales, name="list_sales"),
    path("sales/<int:pk>/", sale, name="sale"),
    path("automobileVOs/", list_automobileVOs, name="automobileVOs")
]
