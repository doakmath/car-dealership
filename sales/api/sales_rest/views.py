from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder
from django.http import JsonResponse
import json


# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold"
    ]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "automobile",
        "customer",
        "salesperson",
        "price"
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder()
    }




@require_http_methods(["GET", "POST"])
def list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Salesperson.objects.filter(id=pk).update(**content)
        salesperson = Salesperson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )
    else:
        count, _ = Salesperson.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0})


@require_http_methods(["GET", "POST"])
def list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def customer(request, pk):
    if request.method == "GET":
        customer = Customer.objects.filter(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Customer.objects.filter(id=pk).update(**content)
        customer = Customer.objects.get(id=pk)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
    else:
        count, _ = Customer.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0})

@require_http_methods(["GET", "POST"])
def list_sales(request, VOvin=None):
    if request.method == "GET":
        if VOvin is not None:
            sales = Sale.objects.filter(vin=VOvin)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile

            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson

            customer_phone = content["customer"]
            customer = Customer.objects.get(phone_number=customer_phone)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                { "message": "Invalid VIN, salespersonID, or phoneNumber"},
                status = 400
            )
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder = SaleEncoder,
            safe = False
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def sale(request, pk):
    if request.method == "GET":
        sale = Sale.objects.filter(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        Sale.objects.filter(id=pk).update(**content)
        sale = Sale.objects.get(id=pk)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
        )
    else:
        count, _ = Sale.objects.filter(id=pk).delete()
        return JsonResponse({"deleted:": count > 0})



@require_http_methods(["GET"])
def list_automobileVOs(request):
    if request.method == "GET":
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobileVOs": automobileVOs},
            encoder=AutomobileVOEncoder,
            safe=False
        )
