import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Appointment, Technician
from django.views.decorators.http import require_http_methods
from django.http import HttpResponseBadRequest, HttpResponseNotFound
# Create your views here.

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]
class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "vin",
        "customer",
        "status",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        technician_id = content.pop('technician')

        # if employee_id is None:
        #     return HttpResponseBadRequest("Technician ID is required.")
        try:
            technician = Technician.objects.get(id=technician_id)
        except Technician.DoesNotExist:
            return HttpResponseBadRequest(f"Technician with id {technician_id} does not exist.")
        content['technician'] = technician

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_appointment(request, pk):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=pk).delete()
        return JsonResponse(
            {"Deleted": count > 0}
        )

@require_http_methods(["PUT"])
def cancel_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status="Canceled")
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return HttpResponseNotFound("Appointment not found.")

@require_http_methods(["PUT"])
def finish_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status="Finished")
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
    except Appointment.DoesNotExist:
        return HttpResponseNotFound("Appointment not found.")

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET"])
def api_technician(request, pk):
    if request.method == "GET":
        technician = Technician.objects.get(id=pk)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
