from django.urls import path
from .views import cancel_appointment, finish_appointment, api_appointment, api_appointments, api_technician, api_technicians

urlpatterns = [
    path("appointments/<int:pk>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:pk>/finish/", finish_appointment, name="finish_appointment"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/", api_appointments, name="api_appointments"),
]
